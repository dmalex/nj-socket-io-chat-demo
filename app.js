const express = require("express")
const app = express()
app.get('/', function(req, res){
  res.send('<h1>B.RF Group - SocketChat Server by Dmitry Alexandrov</h1>')
})
const server = require("http").createServer(app)
const io = require('socket.io')(server);
const port = 3000
var userList = []
var typingUsers = {}

io.on("connection", socket => {
    console.log("A user connected.")

    socket.on('connect', function(){
      console.log("connect");
    })

    socket.on('ping', function(){
      console.log("ping");
    })

    socket.on("chat message", msg => { // from rn client
        console.log(msg + " from " + "rn client")
        io.emit("chat message", msg) // send to rn client
        const now = new Date().toLocaleString()
        io.emit('newChatMessage', "rn-client", msg, now) // send to swift client        
        console.log(msg)
    } )

    socket.on("chatMessage", function(clientNickname, message){ // from swift client
      console.log(message + " from " + clientNickname)

      const currentDateTime = new Date().toLocaleString()
      delete typingUsers[clientNickname]
      io.emit("userTypingUpdate", typingUsers)
      io.emit('newChatMessage', clientNickname, message, currentDateTime) // to swift client
      io.emit('chat message', message + " from " + clientNickname + "@ " + currentDateTime) // to rn client
    })
 
    socket.on("startType", function(clientNickname){
      console.log("User " + clientNickname + " is typing...")
      typingUsers[clientNickname] = 1
      io.emit("userTypingUpdate", typingUsers)
    })
  
  
    socket.on("stopType", function(clientNickname){
      console.log("User " + clientNickname + " has stopped typing...");
      delete typingUsers[clientNickname];
      io.emit("userTypingUpdate", typingUsers);
    })


    socket.on("connectUser", function(clientNickname) {
      var message = "User " + clientNickname + " was connected."
      console.log(message)

      var userInfo = {}
      var foundUser = false
      for ( var i = 0; i < userList.length; i++ ) {
        if ( userList[i]["nickname"] == clientNickname) {
          userList[i]["isConnected"] = true
          userList[i]["id"] = socket.id
          userInfo = userList[i]
          foundUser = true
          break
        }
      }

      if ( !foundUser ) {
        userInfo["id"] = socket.id
        userInfo["nickname"] = clientNickname
        userInfo["isConnected"] = true
        userList.push(userInfo)
      }

      io.emit("userList", userList)
      io.emit("userConnectUpdate", userInfo)
  })


  socket.on("exitUser", function(clientNickname){
    for ( var i = 0; i  < userList.length; i++ ) {
      if ( userList[i]["id"] == socket.id ) {
        userList.splice(i, 1)
        break
      }
    }
    io.emit("userExitUpdate", clientNickname)
    console.log('exitUser')

  })


  socket.on('disconnect', function(){
    console.log('User disconnected')

    var clientNickname
    for ( var i = 0; i < userList.length; i++ ) {
      if ( userList[i]["id"] == socket.id ) {
        userList[i]["isConnected"] = false
        clientNickname = userList[i]["nickname"]
        break
      }
    }

    delete typingUsers[clientNickname]
    io.emit("userList", userList)
    io.emit("userExitUpdate", clientNickname)
    io.emit("userTypingUpdate", typingUsers)
  })
})

const showLog = () => {
    console.log("Server running on port " + port)
}
server.listen(port, showLog)
